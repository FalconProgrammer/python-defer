import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fp_defer",
    version="0.0.2",
    author="Jonathan Boyle",
    author_email="programmerfalcon@gmail.com",
    description="Implements a simple go-style defer statement for python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/FalconProgrammer/python-defer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
